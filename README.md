# Installation

`remotes::install_gitlab("jonovik/bingo")`

# Testing

A 3 x 3 grid with random letters (using the built-in variable `LETTERS`):

```
library(bingo)
bingo(LETTERS, 3, 3)
```

The resulting plot can be saved with `ggplot2::gsave()`.